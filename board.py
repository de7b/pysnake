
from tkinter import Tk, Canvas
from computer_mind import *
from images import *


def main():
    root = Tk()
    root.title("Snake")

    winWidth: int = 1520
    winHeight: int = 1070
    winBgColor: str = "lightblue"
    x: int = (root.winfo_screenwidth() - winWidth) / 2
    y: int = (root.winfo_screenheight() - winHeight) / 2

    root.geometry('%dx%d+%d+%d' % (winWidth, winHeight, x, y))
    root.resizable(False, False)

    canvas = Canvas(root, width=winWidth, height=winHeight, bg=winBgColor)

    config = PainterConfig()
    config.Size = winHeight
    config.BoardBgColor = "white"
    config.Offset = 10
    config.CellSize = 50
    config.DrawGrid = False
    config.GridLineColor = "blue"
    config.God_mod = False
    config.SnakeImages = build_snake_images(config.CellSize)
    config.MeelImages = build_meel_images(config.CellSize)
    config.Satietys = [1, 4, 10]
    config.Plus_speed = False

    cells = int((winHeight - 2 * config.Offset)/config.CellSize)

    painter = Painter(canvas, config)

    snake = Snake(10, 'Epic', SnakeDirection.RIGHT, 2, [1, 10], 1, [-1, -1])
    comp = Snake(5, 'Rare', SnakeDirection.RIGHT, 1, [1, 20], 0, [-1, -1])
    second_comp = Snake(5, 'Rare', SnakeDirection.UP, 1, [10, 20], 0, [-1, -1])
    f_comp = Snake(5, 'Common', SnakeDirection.RIGHT, 1, [10, 40], 0, [-1, -1])
    s_comp = Snake(5, 'Common', SnakeDirection.DOWN, 1, [30, 40], 0, [-1, -1])
    # t_comp = Snake(5, 'Common', SnakeDirection.UP, 1, [30, 20], 0, [-1, -1])

    snakes_massiv = [snake, comp, second_comp, f_comp, s_comp]
    computer_massiv = [comp, second_comp, f_comp, s_comp]

    canvas.pack()

    def create_snake(zmeya):
        zmeya.Cells.append(painter.createCell(zmeya.Place[0], zmeya.Place[1], 'tail', 1, zmeya.Skin))
        for i in range(zmeya.Size - 2):
                zmeya.Cells.append(painter.createCell(zmeya.Cells[-1].X + 1, zmeya.Cells[-1].Y, 'body', 1, zmeya.Skin))
        zmeya.Cells.append(painter.createCell(zmeya.Cells[-1].X + 1, zmeya.Cells[-1].Y, 'head', 1, zmeya.Skin))

    for i in range(len(snakes_massiv)):
        create_snake(snakes_massiv[i])

    def left(event):
        if snake.Cells[-1].X - 1 == snake.Cells[-2].X and snake.Cells[-1].Y == snake.Cells[-2].Y:
            return
        snake.Direction = SnakeDirection.LEFT
        rotate_cell(snake.Cells[-1], 3)
        snake.Cells[-1].Direction = 3

    def right(evnet):
        if snake.Cells[-1].X + 1 == snake.Cells[-2].X and snake.Cells[-1].Y == snake.Cells[-2].Y:
            return
        snake.Direction = SnakeDirection.RIGHT
        rotate_cell(snake.Cells[-1], 1)
        snake.Cells[-1].Direction = 1

    def up(event):
        if snake.Cells[-1].X == snake.Cells[-2].X and snake.Cells[-1].Y - 1 == snake.Cells[-2].Y:
            return
        snake.Direction = SnakeDirection.UP
        rotate_cell(snake.Cells[-1], 0)
        snake.Cells[-1].Direction = 0

    def down(event):
        if snake.Cells[-1].X == snake.Cells[-2].X and snake.Cells[-1].Y + 1 == snake.Cells[-2].Y:
            return
        snake.Direction = SnakeDirection.DOWN
        rotate_cell(snake.Cells[-1], 2)
        snake.Cells[-1].Direction = 2

    def space(event):
        if config.God_mod:
            config.God_mod = False
        else:
            config.God_mod = True

    def plus(event):
        if snake.Speed != 10:
            snake.Speed += 1

    def mines(event):
        if snake.Speed != 1:
            snake.Speed -= 1

    def teleport(snake_tip):
        if snake_tip.Cells[-1].X >= (config.Size - 2 * config.Offset) / config.CellSize:
            painter.moveCell(snake_tip.Cells[-1].Id, -((config.Size - 2 * config.Offset) / config.CellSize), 0)
            snake_tip.Cells[-1].X = 0
        elif snake_tip.Cells[-1].Y >= (config.Size - 2 * config.Offset) / config.CellSize:
            painter.moveCell(snake_tip.Cells[-1].Id, 0, -snake_tip.Cells[-1].Y)
            snake_tip.Cells[-1].Y = 0
        if snake_tip.Cells[-1].X <= -1:
            painter.moveCell(snake_tip.Cells[-1].Id, (config.Size - 2 * config.Offset) / config.CellSize, 0)
            snake_tip.Cells[-1].X = (config.Size - 2 * config.Offset) / config.CellSize - 1
        elif snake_tip.Cells[-1].Y <= -1:
            painter.moveCell(snake_tip.Cells[-1].Id, 0, (config.Size - 2 * config.Offset) / config.CellSize)
            snake_tip.Cells[-1].Y = (config.Size - 2 * config.Offset) / config.CellSize - 1

    canvas.focus_set()
    canvas.bind('<Left>', left)
    canvas.bind('<Right>', right)
    canvas.bind('<Up>', up)
    canvas.bind('<Down>', down)
    canvas.bind('<space>', space)
    canvas.bind('<w>', plus)
    canvas.bind('<q>', mines)

    food_massiv = []

    def create_food():
        a = random.randint(0, 3)
        if a == 1:
            x = random.randint(0, int((winHeight - 2 * config.Offset) / config.CellSize) - 1)
            y = random.randint(0, int((winHeight - 2 * config.Offset) / config.CellSize) - 1)
            b = random.randint(0, 16)
            if b < 10:
                color = "brown"
                satiety = config.Satietys[0]
            elif 10 <= b < 15:
                color = "green"
                satiety = config.Satietys[1]
            else:
                color = "red"
                satiety = config.Satietys[2]

            meel = Food(satiety, color, x, y)

            if cell_check(meel.X, meel.Y, snakes_massiv) == 1:
                del meel
                return
            meel.Id = painter.createFood(meel.X, meel.Y, meel.Satiety)
            food_massiv.append(meel)

    def snake_die(obect):
        if obect == snake and config.God_mod:
            return
        for i in range(len(snakes_massiv)):
            if snakes_massiv[i] == obect:
                a = -1
            else:
                a = 0
            for j in range(len(snakes_massiv[i].Cells) + a):
                if snakes_massiv[i].Cells[j].X == obect.Cells[-1].X and snakes_massiv[i].Cells[j].Y == obect.Cells[-1].Y:
                    if obect == snake:
                        f = open('text.txt', 'r')
                        stroka = f.read()
                        a = 0
                        for i in stroka:
                            if i == ' ':
                                record = stroka[0:a + 1]
                            a += 1
                        if len(snake.Cells) > int(record):
                            f.close()
                            f = open('text.txt', 'w')
                            f.write('%s %s' % (len(snake.Cells), int(str(len(snake.Cells))[0]) ** 4 + 8388608))
                            f.close()
                    if len(snakes_massiv) == 0:
                        print("Очки:%s" % (len(obect.Cells)))
                        exit()
                    else:
                        for a in range(len(snakes_massiv[snakes_massiv.index(obect)].Cells)):
                            canvas.delete(snakes_massiv[snakes_massiv.index(obect)].Cells[a].Id)
                        print('Очки %s змейки: %s' % (obect.Skin, len(obect.Cells)))
                        del snakes_massiv[snakes_massiv.index(obect)]
                        if obect in computer_massiv:
                            del computer_massiv[computer_massiv.index(obect)]
                        return 1

    def snake_eat(snake_tip):
        for i in range(len(food_massiv)):
            if snake_tip.Cells[-1].X == food_massiv[i].X and snake_tip.Cells[-1].Y == food_massiv[i].Y:
                snake_tip.Growth += food_massiv[i].Satiety
                canvas.delete(food_massiv[i].Id)
                del food_massiv[i]
                return

    timer = 0

    def control_summa():
        f = open('text.txt', 'r')
        stroka = f.read()
        a = 0
        for i in stroka:
            if i == ' ':
                record = stroka[0:a+1]

                summa = stroka[a+1:]
                break
            a += 1
        need = int(record[0]) ** 4 + 8388608
        if need == int(summa):
            return 1
        else:
            return 0

    if control_summa() == 1:
        painter.recordText()

    def snakes_go():
        for i in range(len(snakes_massiv)):
            snake_now = snakes_massiv[i]
            first_x_old = snake_now.Cells[-1].X
            first_y_old = snake_now.Cells[-1].Y
            second_x_old = snake_now.Cells[1].X
            second_y_old = snake_now.Cells[1].Y

            painter.moveCellto(
                snake_now.Cells[-1].Id,
                snake_now.Cells[-1].X, snake_now.Cells[-1].Y,
                snake_now.Direction.applyX(snake_now.Cells[-1].X), snake_now.Direction.applyY(snake_now.Cells[-1].Y)
            )

            snake_now.Cells[-1].X = snake_now.Direction.applyX(snake_now.Cells[-1].X)
            snake_now.Cells[-1].Y = snake_now.Direction.applyY(snake_now.Cells[-1].Y)
            snake_now.Cells.append(snake_now.Cells[-1])

            painter.moveCellto(
                snake_now.Cells[1].Id,
                snake_now.Cells[1].X, snake_now.Cells[1].Y,
                first_x_old, first_y_old
            )

            snake_now.Cells[1].X = first_x_old
            snake_now.Cells[1].Y = first_y_old
            snake_now.Cells[-2] = snake_now.Cells[1]

            snake_now.Cells[-2].Body_type = 'body'
            rotate_cell(snake_now.Cells[-2], snake_now.Cells[-1].Direction)
            snake_now.Cells[-2].Direction = snake_now.Cells[-1].Direction

            if snake_now.Growth != 0:
                if snake_now.Cells[2].Body_type != 'turn' and snake_now.Cells[2].Body_type != 'second_turn':
                    snake_now.Cells[1] = painter.createCell(second_x_old, second_y_old, 'body', snake_now.Cells[2].Direction, snake_now.Skin)
                else:
                    if snake_now.Cells[2].X > second_x_old:
                        direct = 1
                    elif snake_now.Cells[2].X < second_x_old:
                        direct = 3
                    elif snake_now.Cells[2].Y > second_y_old:
                        direct = 0
                    else:
                        direct = 2
                    snake_now.Cells[1] = painter.createCell(second_x_old, second_y_old, 'body', direct, snake_now.Skin)
                snake_now.Growth -= 1

            else:
                painter.moveCellto(
                    snake_now.Cells[0].Id,
                    snake_now.Cells[0].X, snake_now.Cells[0].Y,
                    second_x_old, second_y_old
                )

                snake_now.Cells[0].X = second_x_old
                snake_now.Cells[0].Y = second_y_old
                snake_now.Cells[1] = snake_now.Cells[0]
                if snake_now.Cells[2].Body_type == 'body':
                    rotate_cell(snake_now.Cells[1], snake_now.Cells[2].Direction)
                del snake_now.Cells[0]

    def rotate_cell(cell, new_direction):
        if cell in snake.Cells and snake not in snakes_massiv:
            return
        canvas.delete(cell.Id)
        cell.Id = painter.drawImage(cell.X, cell.Y, cell.Body_type, new_direction, cell.Skin)

    computer = ComputerThink(snakes_massiv, food_massiv, cells)

    def build_turns():
        for i in range(len(snakes_massiv)):
            for j in range(1, len(snakes_massiv[i].Cells)-1):
                if j != len(snakes_massiv[i].Cells)-1:

                    x1 = snakes_massiv[i].Cells[j-1].X
                    y1 = snakes_massiv[i].Cells[j+1].Y
                    x2 = snakes_massiv[i].Cells[j+1].X
                    y2 = snakes_massiv[i].Cells[j-1].Y
                    x = snakes_massiv[i].Cells[j].X
                    y = snakes_massiv[i].Cells[j].Y

                    if x1 > x and y1 > y:
                        snakes_massiv[i].Cells[j].Body_type = 'turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 1)

                        if x1 - x > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 2)

                        elif y1 - y > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 0)

                    elif x1 < x and y1 > y:
                        snakes_massiv[i].Cells[j].Body_type = 'turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 2)  # сделать поворот влево_вверх

                        if x - x1 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 1)

                        elif y1 - y > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 3)

                    elif x1 < x and y1 < y:
                        snakes_massiv[i].Cells[j].Body_type = 'turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 3)  # сделать поворот влево_вниз

                        if x - x1 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 0)

                        elif y - y1 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 2)

                    elif x1 > x and y1 < y:
                        snakes_massiv[i].Cells[j].Body_type = 'turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 0)  # сделать поворот вправо_вниз

                        if x1 - x > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 3)

                        elif y - y1 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 1)

                    elif x2 > x and y2 > y:
                        snakes_massiv[i].Cells[j].Body_type = 'second_turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 1)

                        if x2 - x > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 2)

                        elif y2 - y > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 0)

                    elif x2 < x and y2 > y:
                        snakes_massiv[i].Cells[j].Body_type = 'second_turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 2)

                        if x - x2 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 1)

                        elif y2 - y > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 3)

                    elif x2 < x and y2 < y:
                        snakes_massiv[i].Cells[j].Body_type = 'second_turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 3)

                        if x - x2 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 0)

                        elif y - y2 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 2)

                    elif x2 > x and y2 < y:
                        snakes_massiv[i].Cells[j].Body_type = 'second_turn'
                        rotate_cell(snakes_massiv[i].Cells[j], 0)

                        if x2 - x > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 3)

                        elif y - y2 > 1:
                            rotate_cell(snakes_massiv[i].Cells[j], 1)

    def tickTack():
        nonlocal timer

        snakes_go()
        build_turns()

        if snake in snakes_massiv:
            delitel = 50 * snake.Speed

        else:
            delitel = snakes_massiv[0].Speed * 30

        for j in range(0, len(snakes_massiv)):
            teleport(snakes_massiv[j])
            if snake_die(snakes_massiv[j]) == 1:
                break

        for i in range(0, len(computer_massiv)):
            computer.computer_go(computer_massiv[i])
            computer_massiv[i].Cells[-1].Direction = computer_massiv[i].Direction
            rotate_cell(computer_massiv[i].Cells[-1], computer_massiv[i].Cells[-1].Direction)

        for a in range(0, len(snakes_massiv)):
            snake_eat(snakes_massiv[a])

        if len(snakes_massiv) == 0:
            exit()

        create_food()
        timer += 1
        if config.Plus_speed:
            if timer % delitel == 0 and snake.Speed != 20:
                snake.Speed += 1
                timer = 0
        root.after(int(250/snake.Speed), tickTack)

        painter.updateScoresText(len(snake.Cells))

    tickTack()
    root.mainloop()


if __name__ == '__main__':
    main()
