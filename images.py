from PIL import Image, ImageTk


def build_snake_images(cellSize):

    # Common

    first_image = Image.open('Skins//head.png')
    resized_image = first_image.resize(size=(cellSize, cellSize))
    head_image_up = ImageTk.PhotoImage(resized_image)

    second_image = Image.open('Skins//body.png')
    resized_image = second_image.resize(size=(cellSize, cellSize))
    body_image_up = ImageTk.PhotoImage(resized_image)

    third_image = Image.open('Skins//tail.png')
    resized_image = third_image.resize(size=(cellSize, cellSize))
    tail_image_up = ImageTk.PhotoImage(resized_image)

    f_image = Image.open('Skins//head_right.png')
    resized_image = f_image.resize(size=(cellSize, cellSize))
    head_image_right = ImageTk.PhotoImage(resized_image)

    s_image = Image.open('Skins//body_right.png')
    resized_image = s_image.resize(size=(cellSize, cellSize))
    body_image_right = ImageTk.PhotoImage(resized_image)

    t_image = Image.open('Skins//tail_right.png')
    resized_image = t_image.resize(size=(cellSize, cellSize))
    tail_image_right = ImageTk.PhotoImage(resized_image)

    first_image_down = Image.open('Skins//head_down.png')
    resized_image = first_image_down.resize(size=(cellSize, cellSize))
    head_image_down = ImageTk.PhotoImage(resized_image)

    second_image_down = Image.open('Skins//body_down.png')
    resized_image = second_image_down.resize(size=(cellSize, cellSize))
    body_image_down = ImageTk.PhotoImage(resized_image)

    second_image_down = Image.open('Skins//tail_down.png')
    resized_image = second_image_down.resize(size=(cellSize, cellSize))
    tail_image_down = ImageTk.PhotoImage(resized_image)

    third_image_left = Image.open('Skins//head_left.png')
    resized_image = third_image_left.resize(size=(cellSize, cellSize))
    head_image_left = ImageTk.PhotoImage(resized_image)

    tail_left = Image.open('Skins//tail_left.png')
    resized_image = tail_left.resize(size=(cellSize, cellSize))
    tail_image_left = ImageTk.PhotoImage(resized_image)

    body_left = Image.open('Skins//body_left.png')
    resized_image = body_left.resize(size=(cellSize, cellSize))
    body_image_left = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//rotate_right_down.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    rotate_image_right_down = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//rotate_down_left.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    rotate_image_down_left = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//rotate_left_up.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    rotate_image_left_up = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//rotate_up_right.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    rotate_image_up_right = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//second_rotate_right_down.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    second_rotate_image_right_down = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//second_rotate_down_left.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    second_rotate_image_down_left = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//second_rotate_left_up.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    second_rotate_image_left_up = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//second_rotate_up_right.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    second_rotate_image_up_right = ImageTk.PhotoImage(resized_image)

    # Common

    # Rare

    rfirst_image = Image.open('Skins//rare_head.png')
    resized_image = rfirst_image.resize(size=(cellSize, cellSize))
    rare_head_image_up = ImageTk.PhotoImage(resized_image)

    rsecond_image = Image.open('Skins//rare_body.png')
    resized_image = rsecond_image.resize(size=(cellSize, cellSize))
    rare_body_image_up = ImageTk.PhotoImage(resized_image)

    rthird_image = Image.open('Skins//rare_tail.png')
    resized_image = rthird_image.resize(size=(cellSize, cellSize))
    rare_tail_image_up = ImageTk.PhotoImage(resized_image)

    rf_image = Image.open('Skins//rare_head_right.png')
    resized_image = rf_image.resize(size=(cellSize, cellSize))
    rare_head_image_right = ImageTk.PhotoImage(resized_image)

    rs_image = Image.open('Skins//rare_body_right.png')
    resized_image = rs_image.resize(size=(cellSize, cellSize))
    rare_body_image_right = ImageTk.PhotoImage(resized_image)

    rt_image = Image.open('Skins//rare_tail_right.png')
    resized_image = rt_image.resize(size=(cellSize, cellSize))
    rare_tail_image_right = ImageTk.PhotoImage(resized_image)

    rfirst_image_down = Image.open('Skins//rare_head_down.png')
    resized_image = rfirst_image_down.resize(size=(cellSize, cellSize))
    rare_head_image_down = ImageTk.PhotoImage(resized_image)

    rs_image = Image.open('Skins//rare_body_down.png')
    resized_image = rs_image.resize(size=(cellSize, cellSize))
    rare_body_image_down = ImageTk.PhotoImage(resized_image)

    rsecond_image_down = Image.open('Skins//rare_tail_down.png')
    resized_image = rsecond_image_down.resize(size=(cellSize, cellSize))
    rare_tail_image_down = ImageTk.PhotoImage(resized_image)

    rthird_image_left = Image.open('Skins//rare_head_left.png')
    resized_image = rthird_image_left.resize(size=(cellSize, cellSize))
    rare_head_image_left = ImageTk.PhotoImage(resized_image)

    rs_image = Image.open('Skins//rare_body_left.png')
    resized_image = rs_image.resize(size=(cellSize, cellSize))
    rare_body_image_left = ImageTk.PhotoImage(resized_image)

    rtail_left = Image.open('Skins//rare_tail_left.png')
    resized_image = rtail_left.resize(size=(cellSize, cellSize))
    rare_tail_image_left = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_rotate_right_down.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_rotate_image_right_down = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_rotate_down_left.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_rotate_image_down_left = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_rotate_left_up.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_rotate_image_left_up = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_rotate_up_right.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_rotate_image_up_right = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_second_rotate_right_down.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_second_rotate_image_right_down = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_second_rotate_down_left.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_second_rotate_image_down_left = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_second_rotate_left_up.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_second_rotate_image_left_up = ImageTk.PhotoImage(resized_image)

    rrotate_image = Image.open('Skins//rare_second_rotate_up_right.png')
    resized_image = rrotate_image.resize(size=(cellSize, cellSize))
    rare_second_rotate_image_up_right = ImageTk.PhotoImage(resized_image)

    # Rare

    first_image = Image.open('Skins//epic_head_up.png')
    resized_image = first_image.resize(size=(cellSize, cellSize))
    epic_head_image_up = ImageTk.PhotoImage(resized_image)

    second_image = Image.open('Skins//epic_body_up.png')
    resized_image = second_image.resize(size=(cellSize, cellSize))
    epic_body_image_up = ImageTk.PhotoImage(resized_image)

    third_image = Image.open('Skins//epic_tail_up.png')
    resized_image = third_image.resize(size=(cellSize, cellSize))
    epic_tail_image_up = ImageTk.PhotoImage(resized_image)

    f_image = Image.open('Skins//epic_head_right.png')
    resized_image = f_image.resize(size=(cellSize, cellSize))
    epic_head_image_right = ImageTk.PhotoImage(resized_image)

    s_image = Image.open('Skins//epic_body_right.png')
    resized_image = s_image.resize(size=(cellSize, cellSize))
    epic_body_image_right = ImageTk.PhotoImage(resized_image)

    t_image = Image.open('Skins//epic_tail_right.png')
    resized_image = t_image.resize(size=(cellSize, cellSize))
    epic_tail_image_right = ImageTk.PhotoImage(resized_image)

    first_image_down = Image.open('Skins//epic_head_down.png')
    resized_image = first_image_down.resize(size=(cellSize, cellSize))
    epic_head_image_down = ImageTk.PhotoImage(resized_image)

    second_image_down = Image.open('Skins//epic_body_down.png')
    resized_image = second_image_down.resize(size=(cellSize, cellSize))
    epic_body_image_down = ImageTk.PhotoImage(resized_image)

    second_image_down = Image.open('Skins//epic_tail_down.png')
    resized_image = second_image_down.resize(size=(cellSize, cellSize))
    epic_tail_image_down = ImageTk.PhotoImage(resized_image)

    third_image_left = Image.open('Skins//epic_head_left.png')
    resized_image = third_image_left.resize(size=(cellSize, cellSize))
    epic_head_image_left = ImageTk.PhotoImage(resized_image)

    tail_left = Image.open('Skins//epic_tail_left.png')
    resized_image = tail_left.resize(size=(cellSize, cellSize))
    epic_tail_image_left = ImageTk.PhotoImage(resized_image)

    body_left = Image.open('Skins//epic_body_left.png')
    resized_image = body_left.resize(size=(cellSize, cellSize))
    epic_body_image_left = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//epic_rotate_right_down.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    epic_rotate_image_right_down = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//epic_rotate_down_left.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    epic_rotate_image_down_left = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//epic_rotate_left_up.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    epic_rotate_image_left_up = ImageTk.PhotoImage(resized_image)

    rotate_image = Image.open('Skins//epic_rotate_up_right.png')
    resized_image = rotate_image.resize(size=(cellSize, cellSize))
    epic_rotate_image_up_right = ImageTk.PhotoImage(resized_image)

    # Epic

        #

    # Epic

    # Legendary

        #

    # Legendary

    # Owner_skin

        #

    # Owner_skin

    return [
                [
                    [head_image_up, body_image_up, tail_image_up, rotate_image_up_right, second_rotate_image_up_right],
                    [head_image_right, body_image_right, tail_image_right, rotate_image_right_down, second_rotate_image_right_down],
                    [head_image_down, body_image_down, tail_image_down, rotate_image_down_left, second_rotate_image_down_left],
                    [head_image_left, body_image_left, tail_image_left, rotate_image_left_up, second_rotate_image_left_up]
                ],
                [
                    [rare_head_image_up, rare_body_image_up, rare_tail_image_up, rare_rotate_image_up_right, rare_second_rotate_image_up_right],
                    [rare_head_image_right, rare_body_image_right, rare_tail_image_right, rare_rotate_image_right_down, rare_second_rotate_image_right_down],
                    [rare_head_image_down, rare_body_image_down, rare_tail_image_down, rare_rotate_image_down_left, rare_second_rotate_image_down_left],
                    [rare_head_image_left, rare_body_image_left, rare_tail_image_left, rare_rotate_image_left_up, rare_second_rotate_image_left_up]
                ],
                [
                    [epic_head_image_up, epic_body_image_up, epic_tail_image_up, epic_rotate_image_up_right, epic_rotate_image_up_right],
                    [epic_head_image_right, epic_body_image_right, epic_tail_image_right, epic_rotate_image_right_down,epic_rotate_image_right_down],
                    [epic_head_image_down, epic_body_image_down, epic_tail_image_down, epic_rotate_image_down_left, epic_rotate_image_down_left],
                    [epic_head_image_left, epic_body_image_left, epic_tail_image_left, epic_rotate_image_left_up, epic_rotate_image_left_up]
                ]

            ]


def build_meel_images(cellSize):

    b_apple_image = Image.open('Skins//brown_apple.png')
    resized_image = b_apple_image.resize(size=(cellSize, cellSize))
    brown_apple_image = ImageTk.PhotoImage(resized_image)

    f_apple_image = Image.open('Skins//green_apple.png')
    resized_image = f_apple_image.resize(size=(cellSize, cellSize))
    green_apple_image = ImageTk.PhotoImage(resized_image)

    s_apple_image = Image.open('Skins//red_apple.png')
    resized_image = s_apple_image.resize(size=(cellSize, cellSize))
    red_apple_image = ImageTk.PhotoImage(resized_image)

    return [brown_apple_image, green_apple_image, red_apple_image]
