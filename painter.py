from enum import IntEnum
from tkinter import Canvas, LEFT, NW


class SnakeDirection(IntEnum):
    LEFT = 3
    RIGHT = 1
    UP = 0
    DOWN = 2
    UNKNOWN = 100

    def applyX(self, oldX):
        if self.value == SnakeDirection.LEFT:
            return oldX - 1
        elif self.value == SnakeDirection.RIGHT:
            return oldX + 1
        return oldX

    def applyY(self, oldY):
        if self.value == SnakeDirection.UP:
            return oldY - 1
        elif self.value == SnakeDirection.DOWN:
            return oldY + 1
        return oldY


class Snake:
    Cells: tuple  # места  клеток
    Size: int  # количество клеток
    Skin: str
    Direction: SnakeDirection
    Speed: int  # скорость змейки 1 - 1000
    Place: int
    Growth: int
    Need: tuple

    def __init__(self, size, skin, direction, speed, place, growth, need):
        self.Cells = []
        self.Size = size
        self.Direction = direction
        self.Skin = skin
        self.Speed = speed
        self.Place = place
        self.Growth = growth
        self.Need = need


class Cell:
     X: int
     Y: int
     Body_type: str
     Id: int
     Direction: int
     Skin: str          # Common, Rare, Epic, Legendary, Owner_skin


class Food:
    X: int
    Y: int
    Id: int
    Satiety: int
    Color: str

    def __init__(self, satiety, color, x, y):
        self.Satiety = satiety
        self.Color = color
        self.X = x
        self.Y = y


class PainterConfig:
    Size: int  # размер игрового поля-квадрата без учета отступов
    Offset: int  # размер отступа игрового поля от краев окна
    CellSize: int  # размер ячейки игрового поля в пикселях
    BoardBgColor: str  # цвет игрового поля
    GridLineColor: str  # цвет сетки
    DrawGrid:  bool  # признак того, что нужно рисовать сетку
    God_mod: bool
    SnakeImages: set
    MeelImages: set
    Satietiys: set


class Painter:
    config: PainterConfig  # Смотр
    canvas: Canvas
    textId: object

    def __init__(self, canv, cfg):
        self.canvas = canv
        self.config = cfg
        self.drawBoard()

    def drawBoard(self):
        size: int = self.config.Size - 2 * self.config.Offset
        self.canvas.create_rectangle(
            self.config.Offset, self.config.Offset,
            self.config.Offset + size, self.config.Offset + size,
            fill=self.config.BoardBgColor, width=0
        )

        if self.config.DrawGrid:
            x: int = 0
            while x <= size:
                self.canvas.create_line(
                    x + self.config.Offset, self.config.Offset,
                    x + self.config.Offset, self.config.Offset + size,
                    fill=self.config.GridLineColor, width=0
                )
                self.canvas.create_line(
                    self.config.Offset, self.config.Offset + x,
                    self.config.Offset + size, self.config.Offset + x,
                    fill=self.config.GridLineColor, width=1
                )
                x = x + self.config.CellSize

        self.textId = self.canvas.create_text(2 * self.config.Offset + size, self.config.Offset, text="", justify=LEFT, anchor=NW, font="Verdana 14")
        self.recordId = self.canvas.create_text(2 * self.config.Offset + size, self.config.Offset * 4, text="", justify=LEFT, anchor=NW, font="Verdana 14")

    def updateScoresText(self, scores):
        self.canvas.itemconfigure(self.textId, text="Ваши очки: " + str(scores))

    def recordText(self):
        f = open('text.txt', 'r')
        stroka = f.read()
        a = 0
        for i in stroka:
            if i == ' ':
                record = stroka[0:a+1]
            a += 1
        self.canvas.itemconfigure(self.recordId, text="Ваш рекорд: " + str(record))

    def drawImage(self, x, y, body_type, direction, skin):
        if body_type == 'head':
            body_type = 0
        elif body_type == 'body':
            body_type = 1
        elif body_type == 'tail':
            body_type = 2
        elif body_type == 'turn':
            body_type = 3
        elif body_type == 'second_turn':
            body_type = 4
        if skin == 'Common':
            skin = 0
        elif skin == 'Rare':
            skin = 1
        elif skin == 'Epic':
            skin = 2
        elif skin == 'Legendary':
            skin = 3
        elif skin == 'Owner_skin':
            skin = 4
        return self.canvas.create_image(self.config.Offset + (x + 1 / 2) * self.config.CellSize,
                                 self.config.Offset + (y + 1 / 2) * self.config.CellSize,
                                 image=self.config.SnakeImages[skin][direction][body_type])

    def createCell(self, x, y, body_type, direction, skin):
        cell = Cell()
        cell.X = x
        cell.Y = y
        cell.Body_type = body_type
        cell.Direction = direction
        cell.Skin = skin
        if body_type == 'head':
            massiv_number = 0
        elif body_type == 'body':
            massiv_number = 1
        else:
            massiv_number = 2
        cell.Id = self.drawImage(x, y, massiv_number, direction, skin)
        return cell

    def createFood(self, x, y, satiety):
        if satiety == self.config.Satietys[0]:
            number = 0
        elif satiety == self.config.Satietys[1]:
            number = 1
        else:
            number = 2
        id = self.canvas.create_image(self.config.Offset + (x + 1 / 2) * self.config.CellSize,
                                      self.config.Offset + (y + 1 / 2) * self.config.CellSize,
                                      image=self.config.MeelImages[number])

        return id

    def moveCell(self, cellId, x, y):
        self.canvas.move(cellId, self.config.CellSize * x, self.config.CellSize * y)

    def moveCellto(self, cellid, xold, yold, xnew, ynew):
        self.moveCell(cellid, xnew - xold, ynew - yold)
