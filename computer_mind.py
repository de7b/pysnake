from painter import Painter, PainterConfig, Snake, Food, SnakeDirection
import random


def cell_check(x, y, snakes_massiv):
    for a in range(len(snakes_massiv)):
        for b in range(len(snakes_massiv[a].Cells)):
            if x == snakes_massiv[a].Cells[b].X and y == snakes_massiv[a].Cells[b].Y:
                return 1
    return 0


def small_check(cell, cells, snakes_massiv):
    for_up = -1
    for_right = 1
    for_down = 1
    for_left = -1

    if cell.X >= cells - 1:
        for_right = -cells + 1
    if cell.X <= 1:
        for_left = cells - 1
    if cell.Y >= cells:
        for_down = -cells + 1
    if cell.Y <= 1:
        for_up = cells - 1

    result = [0, 0, 0, 0]

    for a in range(len(snakes_massiv)):
        for b in range(len(snakes_massiv[a].Cells)):
            if cell.X + for_right == snakes_massiv[a].Cells[b].X and cell.Y == snakes_massiv[a].Cells[b].Y:
                result[1] = 1
            elif cell.Y + for_up == snakes_massiv[a].Cells[b].Y and cell.X == snakes_massiv[a].Cells[b].X:
                result[0] = 1
            elif cell.X + for_left == snakes_massiv[a].Cells[b].X and cell.Y == snakes_massiv[a].Cells[b].Y:
                result[3] = 1
            elif cell.Y + for_down == snakes_massiv[a].Cells[b].Y and cell.X == snakes_massiv[a].Cells[b].X:
                result[2] = 1

    return result


def second_check(sn, massiv, snakes_massiv):
    if massiv[0] == 0 and (massiv[1] != 1 or massiv[2] != 1 or massiv[3] != 1) and \
            cell_check(sn.Cells[-1].X + 1, sn.Cells[-1].Y - 1, snakes_massiv) == 1 and cell_check(sn.Cells[-1].X - 1,
                                                                                sn.Cells[-1].Y - 1, snakes_massiv) == 1:
        massiv[0] = 1

    if massiv[1] == 0 and (massiv[0] != 1 or massiv[2] != 1 or massiv[3] != 1) and \
            cell_check(sn.Cells[-1].X + 1, sn.Cells[-1].Y + 1, snakes_massiv) == 1 and cell_check(sn.Cells[-1].X + 1,
                                                                                sn.Cells[-1].Y - 1, snakes_massiv) == 1:
        massiv[1] = 1

    if massiv[2] == 0 and (massiv[1] != 1 or massiv[0] != 1 or massiv[3] != 1) and \
            cell_check(sn.Cells[-1].X + 1, sn.Cells[-1].Y + 1, snakes_massiv) == 1 and cell_check(sn.Cells[-1].X - 1,
                                                                                sn.Cells[-1].Y + 1, snakes_massiv) == 1:
        massiv[2] = 1

    if massiv[3] == 0 and (massiv[1] != 1 or massiv[2] != 1 or massiv[0] != 1) and \
            cell_check(sn.Cells[-1].X - 1, sn.Cells[-1].Y + 1, snakes_massiv) == 1 and cell_check(sn.Cells[-1].X - 1,
                                                                                sn.Cells[-1].Y - 1, snakes_massiv) == 1:
        massiv[3] = 1

    return massiv


def check(head, cells, snakes_massiv):
    itog = small_check(head.Cells[-1], cells, snakes_massiv)
    itog = second_check(head, itog, snakes_massiv)

    return itog


def first_computer_func(comput, massiv):
    new = SnakeDirection.UNKNOWN
    if comput.Direction == SnakeDirection.LEFT and massiv[3] == 1:
        if massiv[0] == 1:
            new = SnakeDirection.DOWN
        elif massiv[2] == 1:
            new = SnakeDirection.UP
        else:
            new = random.choice([SnakeDirection.DOWN, SnakeDirection.UP])

    elif comput.Direction == SnakeDirection.RIGHT and massiv[1] == 1:
        if massiv[0] == 1:
            new = SnakeDirection.DOWN
        elif massiv[2] == 1:
            new = SnakeDirection.UP
        else:
            new = random.choice([SnakeDirection.UP, SnakeDirection.DOWN])

    elif comput.Direction == SnakeDirection.UP and massiv[0] == 1:
        if massiv[1] == 1:
            new = SnakeDirection.LEFT
        elif massiv[3] == 1:
            new = SnakeDirection.RIGHT
        else:
            new = random.choice([SnakeDirection.LEFT, SnakeDirection.RIGHT])

    elif comput.Direction == SnakeDirection.DOWN and massiv[2] == 1:
        if massiv[1] == 1:
            new = SnakeDirection.LEFT
        elif massiv[3] == 1:
            new = SnakeDirection.RIGHT
        else:
            new = random.choice([SnakeDirection.LEFT, SnakeDirection.RIGHT])

    return new


def find_need(face, cells, food_massiv):
    if len(food_massiv) == 0:
        return [-1, -1]

    x = face.Cells[-1].X
    y = face.Cells[-1].Y
    distance_massiv = []

    for i in range(len(food_massiv)):

        first_addend = abs(food_massiv[i].X - x)
        second_addend = abs(food_massiv[i].Y - y)

        if first_addend > int(cells / 2):
            first_addend = cells - first_addend

        if second_addend > int(cells / 2):
            second_addend = cells - second_addend

        distance = first_addend + second_addend
        distance_massiv.append(distance)

    distance_massiv = sorted(distance_massiv)
    need = distance_massiv[0]

    for j in range(len(food_massiv)):
        first_addend = abs(food_massiv[j].X - x)
        second_addend = abs(food_massiv[j].Y - y)

        if first_addend > int(cells / 2):
            first_addend = cells - first_addend

        if second_addend > int(cells / 2):
            second_addend = cells - second_addend

        distance = first_addend + second_addend
        if distance == need:
            need = [food_massiv[j].X, food_massiv[j].Y]
            return need


def find_two_directions(computer, need, cells):
    if need == [-1, -1]:
        return [SnakeDirection.UNKNOWN, SnakeDirection.UNKNOWN]
    x = computer.Cells[-1].X
    y = computer.Cells[-1].Y

    directions = []

    if x > need[0]:
        if x - need[0] > cells / 2:
            directions.append(SnakeDirection.RIGHT)
        else:
            directions.append(SnakeDirection.LEFT)
    elif x < need[0]:
        if need[0] - x > cells / 2:
            directions.append(SnakeDirection.LEFT)
        else:
            directions.append(SnakeDirection.RIGHT)
    else:
        directions.append(SnakeDirection.UNKNOWN)

    if y > need[1]:
        if y - need[1] > cells / 2:
            directions.append(SnakeDirection.DOWN)
        else:
            directions.append(SnakeDirection.UP)
    elif y < need[1]:
        if need[1] - y > cells / 2:
            directions.append(SnakeDirection.UP)
        else:
            directions.append(SnakeDirection.DOWN)
    else:
        directions.append(SnakeDirection.UNKNOWN)

    return directions


def second_computer_func(computer, massiv, food_massiv, cells):
    comp_need = find_need(computer, cells, food_massiv)
    x = computer.Cells[-1].X
    y = computer.Cells[-1].Y

    two_directions = find_two_directions(computer, comp_need, cells)

    if computer.Direction == two_directions[0] or computer.Direction == two_directions[1]:
        return computer.Direction

    if computer.Direction == SnakeDirection.RIGHT or computer.Direction == SnakeDirection.LEFT:
        if two_directions[1] == SnakeDirection.UP:
            if massiv[0] == 0:
                return SnakeDirection.UP

        elif two_directions[1] == SnakeDirection.DOWN:
            if massiv[2] == 0:
                return SnakeDirection.DOWN

    else:
        if two_directions[0] == SnakeDirection.RIGHT:
            if massiv[1] == 0:
                return SnakeDirection.RIGHT

        elif two_directions[0] == SnakeDirection.LEFT:
            if massiv[3] == 0:
                return SnakeDirection.LEFT

    return computer.Direction


class ComputerThink:

    SnakesMassiv: set
    FoodMassiv: set
    Cells: int

    def __init__(self, snakes_massiv, food_massiv, cells):
        self.Snakes_massiv = snakes_massiv
        self.Food_massiv = food_massiv
        self.Cells = cells

    def computer_go(self, computer):
        check_massiv = check(computer, self.Cells, self.Snakes_massiv)

        newDirection: SnakeDirection = computer.Direction

        result = first_computer_func(computer, check_massiv)

        if result != SnakeDirection.UNKNOWN:
            newDirection = result
        else:
            newDirection = second_computer_func(computer, check_massiv, self.Food_massiv, self.Cells)

        computer.Direction = newDirection
